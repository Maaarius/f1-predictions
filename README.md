# F1 Predictions

## TODO

### Driver Features

- [x] Qualifying position
- [x] Driver age at that time
- [x] Years in F1
- [x] Percentage difference in qualifying time from pole position * 100  (e.g. 102.3%)
- [x] Starts in front-row
- [x] Races won in career
- [x] Races won in season till that race
- [x] Races retired
- [x] Races finished
- [x] Pole positions won
- [ ] Drivers championships won
- [ ] Driver championship classification last year
- [x] Drivers championship position this season
- [x] Drivers championship points this season
- [ ] Max, min, avg positions gained/lost during last X races
- [ ] Max, min, avg finishing position in the last X races
- [ ] Correlation between qualifying and race results per driver
- [x] Previous race final position
- [x] Previous race qualifying position
- [ ] Positions gained in previous race
- [ ] Race and Qualifying position in same race last year
- [ ] Positions gained in same race last year
- [ ] Percentage difference from winner (in time) in the last race * 100  (e.g. 102.3%)
- [ ] Number of pit-stops in same race last year
- [ ] Avg lap-time excl. pit stops in last race
- [ ] Avg lap-time consistency excl. pit stops in last race
- [ ] Max/min/avg/std speed in previous race
- [ ] Rank on avg/std of speed in previous race

### Constructors Features

- [ ] Constructors championship won
- [x] Constructors races won
- [x] Constructors races won that season
- [ ] Constructors championship won in last X years
- [ ] Constructors championship classification last year
- [x] Constructors championship position at the time
- [x] Constructors championship points at the time
- [x] Constructors wins that season at the time
- [ ] Max (Team-mate qualifying position, Driver qualifying position)
- [ ] Max, min, avg positions gained/lost during last X races
- [ ] Max, min, avg position in the last X races
- [ ] Percentage difference in top-speed from top in last-race * 100 (e.g. 99.5%)
- [x] Times retired
- [ ] Times retired in last X races
- [ ] Max/min/avg speed in previous race
- [ ] Rank on avg/std of speed in previous race

### Other Features

- [x] Circuit name
- [x] Race rank in season (i.e. 1-21)
- [x] Year
- [x] Racing in home country
- [ ] Average overtakes per race
- [ ] Correlation between race and qualifying results per circuit
